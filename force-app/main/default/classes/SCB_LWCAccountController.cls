public class SCB_LWCAccountController {
    
    public class SObjectWrapper {
        @AuraEnabled public Account accRec {get; set;}
        @AuraEnabled public List<DisplayWrapper> customView {get; set;}
        @AuraEnabled public Boolean hasRecordAccess {get; set;}
    }

    public class DisplayWrapper {
        @AuraEnabled public Object value {get;set;}
        @AuraEnabled public String label {get;set;}
        @AuraEnabled public String type {get;set;}
    }
    
    @AuraEnabled(cacheable=true)
    public static SObjectWrapper getClient(String recId) {
        SObjectWrapper sObjWrap = new SObjectWrapper();
        sObjWrap.accRec = SCB_QueryWithoutSharing.getClientDetails(recId);
        sObjWrap.hasRecordAccess = checkUserAccess(recId);
        return sObjWrap;
    }

    @AuraEnabled(cacheable=false)
    public static Account searchCustomer(String fieldValue, String fieldName) {
        return SCB_QueryWithoutSharing.searchCustomer(fieldValue, fieldName);
    }

    @AuraEnabled(cacheable=false)
    public static List<CustomFieldSearch__mdt> getFieldOptions(){
        return SCB_QueryWithoutSharing.getFieldOptions();
    }
    
    @AuraEnabled(cacheable=false)
    public static SObjectWrapper getRecordView(String recordId, String objectName, String recordType){
        System.debug('recordType: ' + recordType);
        List<Schema.FieldSetMember> fields = getFields(objectName, recordType);

        // query and map here
        Sobject o = SCB_QueryWithoutSharing.getRecordView(fields, objectName, recordId);
        SObjectWrapper sObjWrap = new SObjectWrapper();
        sObjWrap.customView = new List<DisplayWrapper>();
        
        for(Schema.FieldSetMember f : fields) {
            DisplayWrapper t = new DisplayWrapper();
            t.value = o.get(f.getFieldPath());
            t.label = f.getLabel();
            t.type = String.valueOf(f.getType());
            sObjWrap.customView.add(t);
        }
        return  sObjWrap;
    }

    public static List<Schema.FieldSetMember> getFields(String objectName, String recordType) {
        String fieldSetName = 'Popup_Display';
        Map<String, Schema.FieldSet> FsMap = Schema.getGlobalDescribe().get(objectName).getDescribe().FieldSets.getMap();

        // get default field set
        Schema.FieldSet fieldSet = FsMap.get(fieldSetName);

        if(String.isNotBlank(recordType))
            fieldSetName += '_' + recordType;

        if(FsMap.containsKey(fieldSetName))
            fieldSet = FsMap.get(fieldSetName);

        return fieldSet.getFields();
    }

    private static Boolean checkUserAccess(String recId) {
        Boolean hasAccess = false;

        //Iterate list of UserRecordAccess
        for (UserRecordAccess ura : SCB_QueryWithoutSharing.getUserRecordAccess(new List<String>{recId})) {
            //Check if current user has access thru the record
            if (ura.HasReadAccess) {
                hasAccess = true;
            }
        }

        return hasAccess;
    }
}