/*
Description: Used for query withOUT respect sharing rules
*/
public without sharing class SCB_QueryWithoutSharing {

    // NOTE: Make sure to update searchCustomer method with the same field update being done here
    public static Account getClientDetails(String recId){
        return searchCustomer(recId, 'Id');
    }

    public static Account searchCustomer(String fieldValue, String fieldToSearch){
        String queryStr = 'SELECT Name,FinServ__Age__pc,PersonMobilePhone,Segment__c,Qualification_EoM__c,WM_Purchase__c,'
                    + 'FinServ__LastInteraction__c,Total_AUM__c,Owner.Name,FinServ__Status__c,Subsegment__c,Off_us_AUM__c,'
                    + 'FinServ__BranchName__c,PersonBirthdate,FinServ__Citizenship__pc,ID_Type__pc,ID_No__pc,Passport_No__pc,'
                    + 'FinServ__Occupation__pc,FinServ__CurrentEmployer__pc,FinServ__LastReview__c,FinServ__NextReview__c,'
                    + 'FinServ__Gender__pc,FinServ__MaritalStatus__pc,PersonEmail,PersonHomePhone,PersonDoNotCall,'
                    + 'FinServ__CountryOfResidence__pc,US_Resident__pc,US_Citizen__pc,US_Green_Card__pc,'
                    + 'FinServ__RelationshipStartDate__c,Monthly_Income_LCY__pc,Payroll_Active__c,CIP_Rating__c,CIP_Creation_Date__c,'
                    + 'Products_Per_Customer__c,Bill_Payment__c,Credit_Card_Usage_Month__c,Debit_Card__c,General_Insurance__c,CreatedById,'
                    + 'CreatedBy.Name,LastModifiedById,LastModifiedBy.Name,FinServ__TotalInvestments__c,FinServ__TotalBankDeposits__c,'
                    + 'FinServ__WalletShare__c,Relationship_No__c,PersonContactId,is_Affluent_Client__c, FinServ__TotalInvestmentsPrimaryOwner__c,'
                    + 'Total_Banca_Primary_Owner__c, Total_Deposits_Primary_Owner__c, Total_Wealth_Management_Primary_Owner__c,'
                    + 'Total_Loans_Primary_Owner__c, Total_Credit_Card_Primary_Owner__c, Total_Current_Account_Primary_Owner__c,'
                    + 'Total_Mortgage_Primary_Owner__c, Total_Overdraft_Primary_Owner__c, Total_PCI_Primary_Owner__c,'
                    + 'Total_Personal_Loan_Primary_Owner__c, Total_Savings_Account_Primary_Owner__c, Total_Structured_Deposit_Primary_Owner__c,'
                    + 'Total_Time_Deposit_Primary_Owner__c, Total_Investment__c, Total_Bank_Deposit__c'
                    + ' FROM Account'
                    + ' WHERE ' + fieldToSearch + '=:fieldValue'
                    + ' WITH SECURITY_ENFORCED';
        return Database.query(queryStr);
    }

    public static List<AggregateResult> getAggregatedFinancialAccounts(String accountId, Set<String> recordTypeNameSet){
        return new List<AggregateResult>([SELECT SUM(FinServ__Balance__c) sum, 
                                                 SUM(Balance_M_1__c) balanceM1, 
                                                 SUM(Balance_M_2__c) balanceM2, 
                                                 SUM(Balance_M_3__c) balanceM3, 
                                                 SUM(Balance_M_4__c) balanceM4, 
                                                 SUM(Balance_M_5__c) balanceM5, 
                                                 SUM(Balance_M_6__c) balanceM6, 
                                                 SUM(Balance_M_7__c) balanceM7, 
                                                 SUM(Balance_M_8__c) balanceM8, 
                                                 SUM(Balance_M_9__c) balanceM9, 
                                                 SUM(Balance_M_10__c) balanceM10, 
                                                 SUM(Balance_M_11__c) balanceM11, 
                                                 SUM(Balance_M_12__c) balanceM12, 
                                                 RecordType.DeveloperName 
                                          FROM   FinServ__FinancialAccount__c 
                                          WHERE  FinServ__PrimaryOwner__c =: accountId 
                                          AND    RecordType.DeveloperName IN: recordTypeNameSet 
                                          AND    FinServ__Managed__c = true 
                                          AND    FinServ__Status__c =: SCB_Constants.FINANCIAL_ACCOUNT_STATUS_ACTIVE WITH SECURITY_ENFORCED
                                          GROUP BY RecordType.DeveloperName]);
    }

    public static List<Account_Product__mdt> getAccountProduct(){
        return new List<Account_Product__mdt>([SELECT MasterLabel, 
                                                      DeveloperName, 
                                                      Level_1__c, 
                                                      Financial_Account_Record_Type_API_Name__c 
                                               FROM   Account_Product__mdt]);
    }

    public static List<FinServ__FinancialAccount__c> getFinancialAccounts(String accountId, String recordTypeName){
        return new List<FinServ__FinancialAccount__c>([SELECT Id, 
                                                              Name, 
                                                              FinServ__Balance__c, 
                                                              FinServ__LoanEndDate__c, 
                                                              RecordType.DeveloperName 
                                                       FROM   FinServ__FinancialAccount__c 
                                                       WHERE  FinServ__PrimaryOwner__c =: accountId 
                                                       AND    RecordType.DeveloperName =: recordTypeName 
                                                       AND    FinServ__Managed__c = true 
                                                       AND    FinServ__Status__c =: SCB_Constants.FINANCIAL_ACCOUNT_STATUS_ACTIVE WITH SECURITY_ENFORCED]);
    }

    public static Sobject getRecordView(List<Schema.FieldSetMember> objFieldSet, String objectName, String recordId){
        String queryStr = 'SELECT ';
        Boolean isFirst = true;
        for(Schema.FieldSetMember f : objFieldSet) {
            if(isFirst){
                isFirst = false;
            } else {
                queryStr += ',';
            }
            queryStr += f.getFieldPath();
        }
        queryStr += ' FROM ' + objectName + ' WHERE Id=:recordId WITH SECURITY_ENFORCED';

        return Database.query(queryStr);
    }

    public static List<CustomFieldSearch__mdt> getFieldOptions(){
        return [SELECT MasterLabel, FieldAPIName__c, IsDefault__c FROM CustomFieldSearch__mdt ORDER BY MasterLabel ASC];
    }

    public static List<UserRecordAccess> getUserRecordAccess(List<String> recIds) {
        List<UserRecordAccess> resultList = new List<UserRecordAccess>();

        //Iterate list user record access based on the current user
        for (UserRecordAccess userRecAccss : [SELECT RecordId, 
                                                    HasReadAccess
                                            FROM UserRecordAccess
                                            WHERE UserId =: UserInfo.getUserId()
                                            AND RecordId IN :recIds]) {
            resultList.add(userRecAccss);
        }

        return resultList;
    }

    public static Trigger_Control__mdt getTriggerControl(String name){
        return [SELECT Active__c FROM Trigger_Control__mdt WHERE DeveloperName =: name LIMIT 1];
    }

    public static Map<Id, Account> getClients(Set<Id> idSet){
        return new Map<Id, Account>([SELECT Id, Relationship_No__c FROM Account WHERE Id IN: idSet]);
    }
}