@isTest
private class SCB_ClientAccountDetailsController_Test {
    @testSetup static void staticRecords(){
    
    }
    
    @isTest static void getFinancialAccountsMix_test() {
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(1);
        Account personAccount = accountList.get(0);
        insert personAccount;
        
        List<FinServ__FinancialAccount__c> financialAccountList = new List<FinServ__FinancialAccount__c>();

        Schema.DescribeSObjectResult describeObject = Schema.SObjectType.FinServ__FinancialAccount__c;
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeObject.getRecordTypeInfosById();
        for(Id recordTypeId: recordTypeMap.keySet()){
            if(recordTypeMap.get(recordTypeId).isActive() && 
               !recordTypeMap.get(recordTypeId).isMaster()){
                financialAccountList.add(SCB_TestDataFactory.createFinancialAccount(recordTypeId, personAccount.Id));
            }
        }
        insert financialAccountList;

        Map<String, Set<String>> segmentMap = SCB_ClientAumMixController.getAccountSegment();
        Map<String, String> recordTypeNameMap = new Map<String, String>();
        for(Account_Product__mdt ap: SCB_QueryWithoutSharing.getAccountProduct()){
            recordTypeNameMap.put(ap.MasterLabel, ap.Financial_Account_Record_Type_API_Name__c);
        }

        Test.StartTest();
        Map<String, Map<Decimal, List<SCB_ClientAccountDetailsController.DataSet>>> dataMap = SCB_ClientAccountDetailsController.getSegments(personAccount.Id);
        for(String segment: dataMap.keySet()){
            System.assert(segmentMap.keySet().contains(segment));
            if(segmentMap.keySet().contains(segment)){
                for(Decimal d: dataMap.get(segment).keySet()){
                    System.assertNotEquals(0, d);
                    for(SCB_ClientAccountDetailsController.DataSet ds: dataMap.get(segment).get(d)){
                        System.assert(segmentMap.get(segment).contains(recordTypeNameMap.get(ds.accountType)));
                        System.assertNotEquals(0, ds.balance);
                    }
                }
            }
        }
        Test.StopTest();
    }

    @isTest static void getFinancialAccounts_test() {
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(1);
        Account personAccount = accountList.get(0);
        insert personAccount;
        
        List<FinServ__FinancialAccount__c> financialAccountList = new List<FinServ__FinancialAccount__c>();

        Schema.DescribeSObjectResult describeObject = Schema.SObjectType.FinServ__FinancialAccount__c;
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeObject.getRecordTypeInfosById();
        for(Id recordTypeId: recordTypeMap.keySet()){
            if(recordTypeMap.get(recordTypeId).isActive() && 
               !recordTypeMap.get(recordTypeId).isMaster()){
                financialAccountList.add(SCB_TestDataFactory.createFinancialAccount(recordTypeId, personAccount.Id));
            }
        }
        
        financialAccountList.add(SCB_TestDataFactory.createFinancialAccount(Schema.SObjectType.FinServ__FinancialAccount__c.getRecordTypeInfosByName().get('Investment Account').getRecordTypeId(), personAccount.Id));
        insert financialAccountList;

        Map<String, String> recordTypeNameMap = new Map<String, String>();
        for(Account_Product__mdt ap: SCB_QueryWithoutSharing.getAccountProduct()){
            recordTypeNameMap.put(ap.MasterLabel, ap.Financial_Account_Record_Type_API_Name__c);
        }

        Test.StartTest();
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_INVESTMENT);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_PCI);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_STRUCTURED_DEPOSIT);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_CURRENT_ACCOUNT);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_SAVINGS_ACCOUNT);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_TIME_DEPOSIT);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_OVERDRAFT);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_MORTGAGE);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_PERSONAL_LOAN);
        validateFinancialAccount(personAccount.Id, SCB_Constants.ACCOUNT_TYPE_CREDIT_CARD);
        Test.StopTest();
    }

    public static void validateFinancialAccount(Id accountId, String recordTypeName){
        for(SCB_ClientAccountDetailsController.DataSet ds: SCB_ClientAccountDetailsController.getFinancialAccounts(accountId, recordTypeName).get(recordTypeName)){
            System.assertEquals(100000, ds.balance);
        }
    }
}