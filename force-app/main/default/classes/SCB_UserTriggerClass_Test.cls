@isTest
private class SCB_UserTriggerClass_Test {
    @testSetup static void staticRecords(){

    }
    
    @isTest static void insertUser_Test() {
        UserRole bmRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'X0240' LIMIT 1];
        Profile bmProfile = [SELECT Id FROM Profile WHERE Name =: Label.SCB_Affluent_Sales_BM_Profile LIMIT 1];
        
        List<User> userList = SCB_TestDataFactory.createUsers(bmRole.Id, bmProfile.Id, 10);

        Test.StartTest();
        insert userList;
		Test.StopTest();
        
        Set<Id> userIdSet = new Set<Id>();
        for(User u: userList){
            userIdSet.add(u.Id);
        }

        Group bmGroup = [SELECT Id FROM GROUP WHERE DeveloperName =: SCB_Constants.PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP LIMIT 1];
        Set<Id> groupMemberIdSet = new Set<Id>();
        for(GroupMember gm: [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: bmGroup.Id]){
            groupMemberIdSet.add(gm.UserOrGroupId);
        }

        for(Id userId: userIdSet){
            System.assert(groupMemberIdSet.contains(userId));
        }
    }

    @isTest static void updateUser_Test() {
        UserRole bmRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'X0240' LIMIT 1];
        Profile rmProfile = [SELECT Id FROM Profile WHERE Name = 'Affluent Sales RM' LIMIT 1];
        Profile bmProfile = [SELECT Id FROM Profile WHERE Name =: Label.SCB_Affluent_Sales_BM_Profile LIMIT 1];
        
        List<User> userList = SCB_TestDataFactory.createUsers(bmRole.Id, rmProfile.Id, 10);
        insert userList;
        
        for(User u: userList){
            u.ProfileId = bmProfile.Id;
        }

        Test.StartTest();
        update userList;
		Test.StopTest();
        
        Set<Id> userIdSet = new Set<Id>();
        for(User u: userList){
            userIdSet.add(u.Id);
        }

        Group bmGroup = [SELECT Id FROM GROUP WHERE DeveloperName =: SCB_Constants.PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP LIMIT 1];
        Set<Id> groupMemberIdSet = new Set<Id>();
        for(GroupMember gm: [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: bmGroup.Id]){
            groupMemberIdSet.add(gm.UserOrGroupId);
        }

        for(Id userId: userIdSet){
            System.assert(groupMemberIdSet.contains(userId));
        }
    }

    @isTest static void updateUserRemove_Test() {
        UserRole bmRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'X0240' LIMIT 1];
        Profile rmProfile = [SELECT Id FROM Profile WHERE Name = 'Affluent Sales RM' LIMIT 1];
        Profile bmProfile = [SELECT Id FROM Profile WHERE Name =: Label.SCB_Affluent_Sales_BM_Profile LIMIT 1];
        
        List<User> userList = SCB_TestDataFactory.createUsers(bmRole.Id, bmProfile.Id, 10);
        insert userList;
        
        for(User u: userList){
            u.ProfileId = rmProfile.Id;
        }
        Test.StartTest();
        update userList;
		Test.StopTest();
        
        Set<Id> userIdSet = new Set<Id>();
        for(User u: userList){
            userIdSet.add(u.Id);
        }

        Group bmGroup = [SELECT Id FROM GROUP WHERE DeveloperName =: SCB_Constants.PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP LIMIT 1];
        Set<Id> groupMemberIdSet = new Set<Id>();
        for(GroupMember gm: [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: bmGroup.Id]){
            groupMemberIdSet.add(gm.UserOrGroupId);
        }

        for(Id userId: userIdSet){
            System.assert(!groupMemberIdSet.contains(userId));
        }
    }

    @isTest static void updateUserExisting_Test() {
        UserRole bmRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'X0240' LIMIT 1];
        Profile rmProfile = [SELECT Id FROM Profile WHERE Name = 'Affluent Sales RM' LIMIT 1];
        Profile bmProfile = [SELECT Id FROM Profile WHERE Name =: Label.SCB_Affluent_Sales_BM_Profile LIMIT 1];
        
        List<User> userList = SCB_TestDataFactory.createUsers(bmRole.Id, rmProfile.Id, 10);
        insert userList;
        
        Group bmGroup = [SELECT Id FROM GROUP WHERE DeveloperName =: SCB_Constants.PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP LIMIT 1];
        List<GroupMember> groupMemberListAdd = new List<GroupMember>(); 
        
        for(User u: userList){
            u.ProfileId = bmProfile.Id;
            GroupMember groupMem = new GroupMember(); 
            groupMem.GroupId = bmGroup.Id;
            groupMem.UserOrGroupId = u.Id;
            groupMemberListAdd.add(groupMem);
        }

        insert groupMemberListAdd;

        Test.StartTest();
        update userList;
		Test.StopTest();
        
        Set<Id> userIdSet = new Set<Id>();
        for(User u: userList){
            userIdSet.add(u.Id);
        }

        Set<Id> groupMemberIdSet = new Set<Id>();
        for(GroupMember gm: [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: bmGroup.Id]){
            groupMemberIdSet.add(gm.UserOrGroupId);
        }

        for(Id userId: userIdSet){
            System.assert(groupMemberIdSet.contains(userId));
        }
    }

    @isTest static void updateUserDeactivate_Test() {
        UserRole bmRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'X0240' LIMIT 1];
        Profile bmProfile = [SELECT Id FROM Profile WHERE Name =: Label.SCB_Affluent_Sales_BM_Profile LIMIT 1];
        
        List<User> userList = SCB_TestDataFactory.createUsers(bmRole.Id, bmProfile.Id, 10);
        insert userList;
        
        for(User u: userList){
            u.isActive = false;
        }
        Test.StartTest();
        update userList;
		Test.StopTest();
        
        Set<Id> userIdSet = new Set<Id>();
        for(User u: userList){
            userIdSet.add(u.Id);
        }

        Group bmGroup = [SELECT Id FROM GROUP WHERE DeveloperName =: SCB_Constants.PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP LIMIT 1];
        Set<Id> groupMemberIdSet = new Set<Id>();
        for(GroupMember gm: [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: bmGroup.Id]){
            groupMemberIdSet.add(gm.UserOrGroupId);
        }

        for(Id userId: userIdSet){
            System.assert(!groupMemberIdSet.contains(userId));
        }
    }

    @isTest static void updateUserActivate_Test() {
        UserRole bmRole = [SELECT Id FROM UserRole WHERE DeveloperName = 'X0240' LIMIT 1];
        Profile bmProfile = [SELECT Id FROM Profile WHERE Name =: Label.SCB_Affluent_Sales_BM_Profile LIMIT 1];
        Organization org = [SELECT Id FROM Organization LIMIT 1];   
        
        List<User> userList = SCB_TestDataFactory.createUsers(bmRole.Id, bmProfile.Id, 10);
        insert userList;
        
        for(User u: userList){
            u.isActive = true;
        }
        
        Test.StartTest();
        update userList;
		Test.StopTest();
        
        Set<Id> userIdSet = new Set<Id>();
        for(User u: userList){
            userIdSet.add(u.Id);
        }

        Group bmGroup = [SELECT Id FROM GROUP WHERE DeveloperName =: SCB_Constants.PUBLIC_GROUP_AFFLUENT_SALES_BM_GROUP LIMIT 1];
        Set<Id> groupMemberIdSet = new Set<Id>();
        for(GroupMember gm: [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: bmGroup.Id]){
            groupMemberIdSet.add(gm.UserOrGroupId);
        }

        for(Id userId: userIdSet){
            System.assert(groupMemberIdSet.contains(userId));
        }
    }
}