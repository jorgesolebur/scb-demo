@isTest
public class SCB_TestDataFactory {
    
    public static List<Account> createPersonAccounts(Integer numberOfRecords){
        List<Account> accountList = new List<Account>();

        for(Integer i = 0; i < numberOfRecords; i++){
            Account acc = new Account();
            acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
            acc.FirstName = 'Test';
            acc.LastName = 'Test'+i;
            acc.Relationship_No__c = '12345'+i;
            acc.FinServ__TotalInvestmentsPrimaryOwner__c = 100000;
            acc.Total_PCI_Primary_Owner__c = 100000;
            acc.Total_Structured_Deposit_Primary_Owner__c = 100000;
            acc.Total_Current_Account_Primary_Owner__c = 100000;
            acc.Total_Savings_Account_Primary_Owner__c = 100000;
            acc.Total_Time_Deposit_Primary_Owner__c = 100000;
            acc.Total_Banca_Primary_Owner__c = 100000;
            acc.Total_Overdraft_Primary_Owner__c = 100000;
            acc.Total_Mortgage_Primary_Owner__c = 100000;
            acc.Total_Personal_Loan_Primary_Owner__c = 100000;
            acc.Total_Credit_Card_Primary_Owner__c = 100000;
            accountList.add(acc);
        }
        return accountList;
    }

    public static FinServ__FinancialAccount__c createFinancialAccount(Id recordTypeId, Id accountId){
        FinServ__FinancialAccount__c financialAccount = new FinServ__FinancialAccount__c();
        financialAccount.RecordTypeId = recordTypeId;
        financialAccount.FinServ__PrimaryOwner__c = accountId;
        financialAccount.FinServ__Balance__c = 100000;
        financialAccount.Balance_M_1__c = 90000;
        financialAccount.Balance_M_2__c = 80000;
        financialAccount.Balance_M_3__c = 70000;
        financialAccount.Balance_M_4__c = 60000;
        financialAccount.Balance_M_5__c = 50000;
        financialAccount.Balance_M_6__c = 40000;
        financialAccount.Balance_M_7__c = 30000;
        financialAccount.Balance_M_8__c = 20000;
        financialAccount.Balance_M_9__c = 10000;
        financialAccount.Balance_M_10__c = 5000;
        financialAccount.Balance_M_11__c = 2500;
        financialAccount.Balance_M_12__c = 1000;
        financialAccount.FinServ__Managed__c = true;
        financialAccount.FinServ__Status__c = SCB_Constants.FINANCIAL_ACCOUNT_STATUS_ACTIVE;
        return financialAccount;
    }

    public static List<FinServ__FinancialAccount__c> createFinancialAccounts(Id recordTypeId, Id accountId, Integer numberOfRecords){
        List<FinServ__FinancialAccount__c> financialAccountList = new List<FinServ__FinancialAccount__c>();

        for(Integer i = 0; i < numberOfRecords; i++){
            FinServ__FinancialAccount__c financialAccount = new FinServ__FinancialAccount__c();
            financialAccount.RecordTypeId = recordTypeId;
            financialAccount.FinServ__PrimaryOwner__c = accountId;
            financialAccount.FinServ__Balance__c = 100000;
            financialAccount.Balance_M_1__c = 90000;
            financialAccount.Balance_M_2__c = 80000;
            financialAccount.Balance_M_3__c = 70000;
            financialAccount.Balance_M_4__c = 60000;
            financialAccount.Balance_M_5__c = 50000;
            financialAccount.Balance_M_6__c = 40000;
            financialAccount.Balance_M_7__c = 30000;
            financialAccount.Balance_M_8__c = 20000;
            financialAccount.Balance_M_9__c = 10000;
            financialAccount.Balance_M_10__c = 5000;
            financialAccount.Balance_M_11__c = 2500;
            financialAccount.Balance_M_12__c = 1000;
            financialAccount.FinServ__Managed__c = true;
            financialAccount.FinServ__Status__c = SCB_Constants.FINANCIAL_ACCOUNT_STATUS_ACTIVE;
            financialAccountList.add(financialAccount);
        }
        return financialAccountList;
    }

    public static Opportunity createOpportunity(Id recordTypeId, Id accountId){
        Opportunity opp = new Opportunity();
        opp.RecordTypeId = recordTypeId;
        opp.AccountId = accountId;
        opp.Amount = 100;
        opp.StageName = 'Close-Won';
        opp.Name = 'Test Opp';
        opp.CloseDate = Date.today();
        return opp;
    }

    public static List<Opportunity> createOpportunities(Id recordTypeId, Id accountId, Integer numberOfRecords){
        List<Opportunity> opportunityList = new List<Opportunity>();

        for(Integer i = 0; i < numberOfRecords; i++){
            Opportunity opp = new Opportunity();
            opp.RecordTypeId = recordTypeId;
            opp.AccountId = accountId;
            opp.Amount = 100;
            opp.StageName = 'Close-Won';
            opp.Name = 'Test Opp';
            opp.CloseDate = Date.today();
            opportunityList.add(opp);
        }
        return opportunityList;
    }

    public static List<User> createUsers(Id roleId, Id profileId, Integer numberOfRecords){
        List<User> userList = new List<User>();
        Organization org = [SELECT Id FROM Organization LIMIT 1];   

        for(integer i = 0; i < 10; i++){
            User testUser = new User();
            testUser.FirstName = 'Test'+i;
            testUser.LastName = 'Test'+i;
            testUser.Alias = 'test'+i;
            testUser.Username = 'test'+i+'@test'+org.Id+'.com';
            testUser.Email = 'test'+i+'@test'+org.Id+'.com';
            testUser.UserRoleId = roleId;
            testUser.ProfileId = profileId;
            testUser.TimeZoneSidKey = 'Asia/Singapore';
            testUser.LocaleSidKey = 'en_SG';
            testUser.LanguageLocaleKey = 'en_US';
            testUser.EmailEncodingKey = 'UTF-8';
            userList.add(testUser);
        }
        return userList;
    }
}