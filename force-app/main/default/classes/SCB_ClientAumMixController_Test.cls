@isTest
private class SCB_ClientAumMixController_Test {
    @testSetup static void staticRecords(){
        List<Account> accountList = SCB_TestDataFactory.createPersonAccounts(1);
        insert accountList;
    }
    
    @isTest static void getFinancialAccountsMix_test() {
        Account personAccount = new Account();
        personAccount.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        personAccount.FirstName = 'Test';
        personAccount.LastName = 'Test';
        personAccount.FinServ__TotalInvestmentsPrimaryOwner__c = 100000;
        personAccount.Total_Current_Account_Primary_Owner__c = 100000;
        personAccount.Total_Banca_Primary_Owner__c = 100000;
        personAccount.Total_Overdraft_Primary_Owner__c = 100000;
        insert personAccount;

        Map<String, Set<String>> segmentMap = SCB_ClientAumMixController.getAccountSegment();

        Test.StartTest();
        for(SCB_ClientAumMixController.DataSet wrapper: SCB_ClientAumMixController.getFinancialAccountsMix(personAccount.Id)){
            System.assert(segmentMap.keySet().contains(wrapper.label));
            if(segmentMap.keySet().contains(wrapper.label)){
                System.assertEquals(100000, wrapper.count);
            }
        }
        Test.StopTest();
    }

    @isTest static void getFinancialAccountsTrend_AUM6M_Test() {
        Account personAccount = [SELECT Id FROM Account WHERE RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId() LIMIT 1];
        
        List<FinServ__FinancialAccount__c> financialAccountList = new List<FinServ__FinancialAccount__c>();

        Schema.DescribeSObjectResult describeObject = Schema.SObjectType.FinServ__FinancialAccount__c;
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeObject.getRecordTypeInfosById();
        for(Id recordTypeId: recordTypeMap.keySet()){
            if(recordTypeMap.get(recordTypeId).isActive() && 
               !recordTypeMap.get(recordTypeId).isMaster()){
                financialAccountList.add(SCB_TestDataFactory.createFinancialAccount(recordTypeId, personAccount.Id));
            }
        }
        insert financialAccountList;

        Map<String, Set<String>> segmentMap = SCB_ClientAumMixController.getAccountSegment();

        Test.StartTest();
        Map<String, Map<String, Decimal>> financialAccountMap = SCB_ClientAumMixController.getFinancialAccountsTrend(personAccount.Id, 6, false);
        System.assertEquals(6, financialAccountMap.keySet().size());

        for(String month: financialAccountMap.keySet()){
            for(String segment: financialAccountMap.get(month).keySet()){
                System.assert(segmentMap.keySet().contains(segment));
                System.assertNotEquals(0, financialAccountMap.get(month).get(segment));
            }
        }
        Test.StopTest();
    }

    @isTest static void getFinancialAccountsTrend_AUM12M_Test() {
        Account personAccount = [SELECT Id FROM Account WHERE RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId() LIMIT 1];
        
        List<FinServ__FinancialAccount__c> financialAccountList = new List<FinServ__FinancialAccount__c>();

        Schema.DescribeSObjectResult describeObject = Schema.SObjectType.FinServ__FinancialAccount__c;
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeObject.getRecordTypeInfosById();
        for(Id recordTypeId: recordTypeMap.keySet()){
            if(recordTypeMap.get(recordTypeId).isActive() && 
               !recordTypeMap.get(recordTypeId).isMaster()){
                financialAccountList.add(SCB_TestDataFactory.createFinancialAccount(recordTypeId, personAccount.Id));
            }
        }
        insert financialAccountList;

        Map<String, Set<String>> segmentMap = SCB_ClientAumMixController.getAccountSegment();

        Test.StartTest();
        Map<String, Map<String, Decimal>> financialAccountMap = SCB_ClientAumMixController.getFinancialAccountsTrend(personAccount.Id, 12, false);
        System.assertEquals(12, financialAccountMap.keySet().size());

        Integer monthCounter = 1;
        for(String month: financialAccountMap.keySet()){
            for(String segment: financialAccountMap.get(month).keySet()){
                System.assert(segmentMap.keySet().contains(segment));
                System.assertNotEquals(0, financialAccountMap.get(month).get(segment));
            }
            monthCounter++;
        }
        Test.StopTest();
    }

    @isTest static void getFinancialAccountsTrend_WM6M_Test() {
        Account personAccount = [SELECT Id FROM Account WHERE RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId() LIMIT 1];
        
        List<FinServ__FinancialAccount__c> financialAccountList = new List<FinServ__FinancialAccount__c>();

        Schema.DescribeSObjectResult describeObject = Schema.SObjectType.FinServ__FinancialAccount__c;
        Map<Id,Schema.RecordTypeInfo> recordTypeMap = describeObject.getRecordTypeInfosById();
        for(Id recordTypeId: recordTypeMap.keySet()){
            if(recordTypeMap.get(recordTypeId).isActive() && 
               !recordTypeMap.get(recordTypeId).isMaster()){
                financialAccountList.add(SCB_TestDataFactory.createFinancialAccount(recordTypeId, personAccount.Id));
            }
        }
        insert financialAccountList;

        Map<String, Set<String>> segmentMap = SCB_ClientAumMixController.getAccountSegment();

        Test.StartTest();
        Map<String, Map<String, Decimal>> financialAccountMap = SCB_ClientAumMixController.getFinancialAccountsTrend(personAccount.Id, 6, true);
        System.assertEquals(6, financialAccountMap.keySet().size());

        for(String month: financialAccountMap.keySet()){
            for(String accountType: financialAccountMap.get(month).keySet()){
                System.assert(segmentMap.get(SCB_Constants.SEGMENT_TYPE_WEALTH_MANAGEMENT).contains(accountType));
                System.assertNotEquals(0, financialAccountMap.get(month).get(accountType));
            }
        }
        Test.StopTest();
    }

    @isTest static void getFinancialAccountsTrend_WM12M_Test() {
        Account personAccount = [SELECT Id FROM Account WHERE RecordTypeId =: Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId() LIMIT 1];
        Map<String, Set<String>> segmentMap = SCB_ClientAumMixController.getAccountSegment();
        
        FinServ__FinancialAccount__c financialAccount = SCB_TestDataFactory.createFinancialAccount(Schema.SObjectType.FinServ__FinancialAccount__c.getRecordTypeInfosByName().get('Investment Account').getRecordTypeId(), personAccount.Id);
        insert financialAccount;

        Test.StartTest();
        Map<String, Map<String, Decimal>> financialAccountMap = SCB_ClientAumMixController.getFinancialAccountsTrend(personAccount.Id, 12, true);
        System.assertEquals(12, financialAccountMap.keySet().size());

        Integer monthCounter = 1;
        for(String month: financialAccountMap.keySet()){
            for(String accountType: financialAccountMap.get(month).keySet()){
                System.assert(segmentMap.get(SCB_Constants.SEGMENT_TYPE_WEALTH_MANAGEMENT).contains(accountType));
                System.assertNotEquals(0, financialAccountMap.get(month).get(accountType));
            }
            monthCounter++;
        }
        Test.StopTest();
    }
}