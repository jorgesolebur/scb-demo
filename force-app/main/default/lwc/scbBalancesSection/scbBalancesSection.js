import { LightningElement, api} from 'lwc';
import getClient from '@salesforce/apex/SCB_LWCAccountController.getClient';

export default class ScbBalancesSection extends LightningElement {
    @api recordId;
    @api accountdata;

    connectedCallback(){

        //If this component is brought from Lightning App Builder only recordId is defined. Therefore we should query tor etrieve accountData as well
        if( (this.accountdata == undefined) && (this.recordId !== undefined)){
            getClient({recId : this.recordId}) 
            .then(data => {
                this.accountdata = data.accRec;
            })
            .catch(error => {
                this.accountdata = undefined;
            });
        } 
    }
}