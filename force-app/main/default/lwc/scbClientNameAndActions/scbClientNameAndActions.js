import { LightningElement, wire, api, track} from 'lwc';
import getClient from '@salesforce/apex/SCB_LWCAccountController.getClient';
import { NavigationMixin } from 'lightning/navigation';

export default class scbClientNameAndActions extends NavigationMixin(LightningElement) {

    @api recordId;
    @api clientData;
    @api addMargin;

    account = "";
    containerClass = 'slds-page-header slds-page-header_record-home'

    connectedCallback(){
        if(this.recordId){
            getClient({recId : this.recordId}) 
            .then(data => {
                this.account = data.accRec;
            })
            .catch(error => {});
        } else if (this.clientData){
            this.account = this.clientData;
        }
        if(this.addMargin){
            this.containerClass = `${this.containerClass} containermargin`; 
        }
    }
}