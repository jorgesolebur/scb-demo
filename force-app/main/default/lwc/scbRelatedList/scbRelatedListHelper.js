import initDataMethod from "@salesforce/apex/SCB_RelatedListController.initData";
import { RecordFieldDataType } from "lightning/uiRecordApi";

export default class ScbRelatedListHelper {

    fetchData(state) {
        let jsonData = Object.assign({}, state)
        //jsonData.numberOfRecords = state.numberOfRecords + 1
        jsonData.records = []
        jsonData = JSON.stringify(jsonData)
        return initDataMethod({ jsonData })
            .then(response => {
                const data = JSON.parse(response)
                return this.processData(data, state)
            })
            .catch(error => {
                console.log(error);
            });
    }

    processData(data, state){
        const records = data.records;
        const visibleRecords = data.visibleRecords;
        const checkApiVisibility = state.checkApiVisibility
        this.generateLinks(records, visibleRecords, checkApiVisibility)
        if (records.length > state.numberOfRecords) {
            records.pop()
        }
        data.title = `${data.sobjectLabelPlural}`
        return data
    }


    initColumnsWithActions(columns, customActions, showActions) {
        if (showActions) {
            customActions = [
                { label: 'Edit', name: 'edit' },
                { label: 'Delete', name: 'delete' }
            ]
        return [...columns, { type: 'action', typeAttributes: { rowActions: customActions } }]
        }
        else {
            return [...columns]
        }
    }

    generateLinks(records, visibleRecords, checkApiVisibility) {
        records.forEach(record => {
            record.LinkName = '/' + record.Id
            for (const propertyName in record) {
                const propertyValue = record[propertyName];
                if (typeof propertyValue === 'object') {
                    const newValue = propertyValue.Id ? ('/' + propertyValue.Id) : null;
                    this.flattenStructure(record, propertyName + '_', propertyValue);
                    if (newValue !== null) {
                        record[propertyName + '_LinkName'] = newValue;
                    }
                }
            }
            
            // check visibility
            var apiFields = checkApiVisibility.split(",")
            apiFields.forEach(af => {
                record[`${af}_isVisible`] = visibleRecords.includes(record[af.replace(".","_")])
            })
        });

    }

    flattenStructure(topObject, prefix, toBeFlattened) {
        for (const propertyName in toBeFlattened) {
            const propertyValue = toBeFlattened[propertyName];
            if (typeof propertyValue === 'object') {
                this.flattenStructure(topObject, prefix + propertyName + '_', propertyValue);
            } else {
                topObject[prefix + propertyName] = propertyValue;
            }
        }
    }
}