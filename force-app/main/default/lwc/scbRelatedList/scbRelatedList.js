import { LightningElement, track, api } from "lwc";
import { NavigationMixin } from "lightning/navigation";
import { encodeDefaultFieldValues } from 'lightning/pageReferenceUtils';
import RelatedListHelper from "./scbRelatedListHelper";
import { loadStyle } from 'lightning/platformResourceLoader';
import relatedListResource from '@salesforce/resourceUrl/relatedListResource';
import labelNewClientReview from '@salesforce/label/c.SCB_New_Client_Review';
import formFactor from '@salesforce/client/formFactor';

export default class ScbRelatedList extends NavigationMixin(LightningElement) {
    @track state = {}
    @api showEdit = false;
    @api showDelete = false;
    @api sobjectApiName;
    @api relatedFieldApiName;
    @api numberOfRecords = 5;
    @api sortedBy;
    @api sortedDirection = "ASC";
    @api rowActionHandler;
    @api fields;
    @api columns;
    @api customActions = [];
    @api headerLabelOverride;
    @api iconOverride;
    @api checkApiVisibility;
    @api hideHeader = false;
    @api showNew = false; // make sure to populate newDefaultFieldValues if this will be going to be enabled
    @api newDefaultFieldValues; // JSON format of fields and values to be pre populated
    @track isClientReview = false;
    @track isCreateClientReview = false;
    
    showTableHeader = true;

    showActions = false;
    isNotDone = true;
    rowOffSet = 0;

    helper = new RelatedListHelper();

    label = {
        labelNewClientReview
    };

    connectedCallback(){ 
        if(formFactor == 'Medium' || 
           formFactor == 'Small'){

            this.showNew = false;
        }

        if(this.sobjectApiName == 'Client_Review__c'){
            this.isClientReview = true;
        }
    }

    renderedCallback() {
        loadStyle(this, relatedListResource + '/relatedList.css');
        if(this.hideHeader){
            this.showTableHeader = false;
        }
    }

    @api
    get recordId() {
        return this.state.recordId;
    }

    set recordId(value) {
        this.state.recordId = value;
        this.init();
    }

    get showActions() {
        return this.showActions
    }
    
    set showActions(value) {
        this.showActions = value;
    }

    get hasRecords() {
        return this.state.records != null && this.state.records.length;
    }

    async init() {
        this.state.showRelatedList = this.recordId != null;
        if (!(this.recordId
            && this.sobjectApiName
            && this.relatedFieldApiName
            && this.fields
            && this.columns)) {
            this.state.records = [];
            return;
        }

        this.state.fields = this.fields
        this.state.relatedFieldApiName = this.relatedFieldApiName
        this.state.recordId = this.recordId
        this.state.numberOfRecords = this.numberOfRecords
        this.state.rowOffSet = this.rowOffSet
        this.state.sobjectApiName = this.sobjectApiName
        this.state.sortedBy = this.sortedBy
        this.state.sortedDirection = this.sortedDirection
        this.state.customActions = this.customActions
        this.state.checkApiVisibility= this.checkApiVisibility ? this.checkApiVisibility : 'Id';

        const data = await this.helper.fetchData(this.state);
        this.state.records = [...this.state.records, ...data.records];

        // stop loading if there is no more record
        if (data.records.length < this.numberOfRecords) {
            this.isNotDone = false
        }

        // override icon
        if (this.iconOverride) this.state.iconName = this.iconOverride;
        else this.state.iconName = data.iconName;

        this.state.sobjectLabel = data.sobjectLabel;
        this.state.sobjectLabelPlural = data.sobjectLabelPlural;

        // override label
        if (this.headerLabelOverride) this.state.title = `${this.headerLabelOverride}`
        else this.state.title = data.title;

        this.state.parentRelationshipApiName = data.parentRelationshipApiName;
        // Third parameter to show/hide datatable actions
        this.state.columns = this.helper.initColumnsWithActions(this.columns, this.customActions, this.showActions)
        return;
    }

    handleRowAction(event) {
        const actionName = event.detail.action.name;
        const row = event.detail.row;

        if (this.rowActionHandler) {
            this.rowActionHandler.call()
        } else {
            switch (actionName) {
                case "delete":
                    this.handleDeleteRecord(row);
                    break;
                case "edit":
                    this.handleEditRecord(row);
                    break;
                default:
            }
        }
    }

    handleDeleteRecord(row) {
        const deleteRecordPopup = this.template.querySelector("c-scb-related-list-delete-popup");
        deleteRecordPopup.recordId = row.Id;
        deleteRecordPopup.recordName = row.Name;
        deleteRecordPopup.sobjectLabel = this.state.sobjectLabel;
        deleteRecordPopup.show();
    }

    handleCreateRecord() {
        if(this.isClientReview){
            this.isCreateClientReview = false;
        }

        const defaultValues = encodeDefaultFieldValues(this.newDefaultFieldValues);
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: this.sobjectApiName,
                actionName: 'new',
            },
            state: {
                nooverride: 1,
                useRecordTypeCheck: 1,
                defaultFieldValues: defaultValues
            }
        });
    }

    handleEditRecord(row) {
        const editRecordPopup = this.template.querySelector("c-scb-related-list-new-edit-popup");
        editRecordPopup.recordId = row.Id;
        editRecordPopup.recordName = row.Name;
        editRecordPopup.sobjectApiName = this.sobjectApiName;
        editRecordPopup.sobjectLabel = this.state.sobjectLabel;
        editRecordPopup.show();
    }

    handleRefreshData() {
        this.init();
    }

    loadMoreData(event) {
        const { target } = event;
        target.isLoading = true;

        this.rowOffSet = this.rowOffSet + this.numberOfRecords;
        this.init()
            .then(() => {
                target.isLoading = false;
            });
    }

    createClientReview(){
        this.isCreateClientReview = true;
    }

    closeModalClientReview(){
        this.isCreateClientReview = false;
    }
}