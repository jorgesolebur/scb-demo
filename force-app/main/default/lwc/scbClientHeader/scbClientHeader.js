import { LightningElement, wire, api} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import getClient from '@salesforce/apex/SCB_LWCAccountController.getClient';

export default class scbClientHeader extends NavigationMixin(LightningElement) {
    @api recordId;
    @api clientData;
    @api addMargin;

    containerClass = 'c-container slds-theme_default';

    account = {Owner:{}};

    connectedCallback(){
        if(this.recordId){
            
            getClient({recId : this.recordId}) 
            .then(data => {
                this.clientId = data.accRec.Id;
                this.account = data.accRec;
            })
            .catch(error => {
                const evt = new ShowToastEvent({
                    title: "Not Found",
                    message: `No record with customer Id ${this.customerId} was found`,
                    variant: "error",
                });
                this.dispatchEvent(evt);
            });
        } else if (this.clientData){
            this.account = this.clientData;
        }

        if(this.addMargin){
            this.containerClass = `${this.containerClass} containermargin`; 
        }
    }

    viewOwner(event) {
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                "recordId": this.account.Owner.Id,
                "objectApiName": "User",
                "actionName": "view"
            },
        });
    }
}