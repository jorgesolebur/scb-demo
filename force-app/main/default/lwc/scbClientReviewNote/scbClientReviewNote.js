import { LightningElement } from 'lwc';
import labelTitle from '@salesforce/label/c.SCB_Client_Review_Note_Title';
import labelSteps from '@salesforce/label/c.SCB_Client_Review_Note_Steps';
import labelNote1 from '@salesforce/label/c.SCB_Client_Review_Note_1';
import labelNote2 from '@salesforce/label/c.SCB_Client_Review_Note_2';
import labelNote3 from '@salesforce/label/c.SCB_Client_Review_Note_3';
import labelNote4 from '@salesforce/label/c.SCB_Client_Review_Note_4';

export default class ScbClientReviewNote extends LightningElement {

    label = {
        labelTitle, 
        labelSteps, 
        labelNote1, 
        labelNote2, 
        labelNote3, 
        labelNote4 
    };
}