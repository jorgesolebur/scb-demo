import { LightningElement,wire,track,api } from 'lwc';
import chartjs from '@salesforce/resourceUrl/ChartJS';
import { loadScript } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getFinancialAccounts from '@salesforce/apex/SCB_ClientAumMixController.getFinancialAccountsMix';
import labelAumMix from '@salesforce/label/c.SCB_AUM_Mix';
import labelNoAccountsFound from '@salesforce/label/c.SCB_AUM_Mix_No_Accounts';

export default class ScbClientAumMix extends LightningElement {
    @api recordId;

    label = {
        labelAumMix, 
        labelNoAccountsFound, 
    };
    
    chart;
    chartjsInitialized = false;
    config={
        type : 'doughnut',
        data :{
            datasets :[
                {
                    data: [
                    ],
                    backgroundColor :[
                    ],
                    label:'Dataset 1'
                }
            ],
            labels:[]
        },
        options: {
            responsive : true,
            legend : {
                position :'bottom'
            },
            animation:{
                animateScale: true,
                animateRotate : true
            }, 
            tooltips: {
                callbacks: {
                  label: function(tooltipItem, data) {
                    var dataLabel = data.labels[tooltipItem.index];
                    var value = ': $' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();
                    if (Chart.helpers.isArray(dataLabel)) {
                      dataLabel = dataLabel.slice();
                      dataLabel[0] += value;
                    } else {
                      dataLabel += value;
                    }
                    return dataLabel;
                  }
                }
            },
        }
    };

    renderedCallback(){
        if(this.chartjsInitialized){
            return;
        }
        this.chartjsInitialized = true;
        Promise.all([
            loadScript(this,chartjs)
        ]).then(() =>{
            const ctx = this.template.querySelector('canvas.donut')
            .getContext('2d');
            this.chart = new window.Chart(ctx, this.config);

            getFinancialAccounts({accountId: this.recordId})
            .then(result => {
                for(var key in result){
                    if(result[key].label == 'Banca'){
                        this.updateChart(result[key].count,result[key].label, 'rgb(255,99,132)');
                    } else if(result[key].label == 'Deposits'){
                        this.updateChart(result[key].count,result[key].label, 'rgb(255,159,64)');
                    } else if(result[key].label == 'Wealth Management'){
                        this.updateChart(result[key].count,result[key].label, 'rgb(255,205,86)');
                    } else if(result[key].label == 'Loans'){
                        this.updateChart(result[key].count,result[key].label, 'rgb(75,192,192)');
                    } 
                }
            })
            .catch(error => {
                
            });
        })
        .catch(error =>{
            this.dispatchEvent(
                new ShowToastEvent({
                    title : 'Error loading ChartJS',
                    message : error.message,
                    variant : 'error',
                }),
            );
        });
    }

    updateChart(count,label,legendColor){
        this.chart.data.labels.push(label);
        this.chart.data.datasets.forEach((dataset) => {
            dataset.data.push(count);
            dataset.backgroundColor.push(legendColor)
        });
        this.chart.update();
    }
}