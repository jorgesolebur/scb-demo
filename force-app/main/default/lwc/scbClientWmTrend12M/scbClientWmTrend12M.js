import { LightningElement,wire,track,api } from 'lwc';
import chartjs from '@salesforce/resourceUrl/ChartJS';
import { loadScript } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getFinancialAccounts from '@salesforce/apex/SCB_ClientAumMixController.getFinancialAccountsTrend';

export default class ScbClientWmTrend12M extends LightningElement {
    @api recordId;

    monthArray = [];
    labelArray = ['Investment Account', 'PCI', 'Structured Deposit'];
    bancaArray = [];
    depositArray = [];
    wealthArray = [];
    
    chart;
    chartjsInitialized = false;
    config={
        type : 'bar',
        data :{
            datasets :[
                {
                    label: this.labelArray[0],
                    data: this.bancaArray,
                    backgroundColor: 'rgb(255,99,132)'
                },
                {
                    label: this.labelArray[1],
                    data: this.depositArray,
                    backgroundColor: 'rgb(255,159,64)'
                },
                {
                    label: this.labelArray[2],
                    data: this.wealthArray,
                    backgroundColor: 'rgb(255,205,86)'
                },
            ],
            labels: this.monthArray
        },
        options: {
            responsive : true,
            legend : {
                position :'bottom'
            },
            animation:{
                animateScale: true,
                animateRotate : true
            }, 
            tooltips: {
                callbacks: {
                  label: function(tooltipItem, data) {
                    var dataLabel = data.datasets[tooltipItem.datasetIndex].label;
                    var value = ': $' + data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index].toLocaleString();
                    if (Chart.helpers.isArray(dataLabel)) {
                      dataLabel = dataLabel.slice();
                      dataLabel[0] += value;
                    } else {
                      dataLabel += value;
                    }
                    return dataLabel;
                  }
                }, 
                mode: 'label'
            },
            scales: {
                xAxes: [{ stacked: true }],
                yAxes: [{ stacked: true }]
            }
        }
    };

    renderedCallback(){
        if(this.chartjsInitialized){
            return;
        }
        this.chartjsInitialized = true;
        Promise.all([
            loadScript(this,chartjs)
        ]).then(() =>{
            const ctx = this.template.querySelector('canvas.wmm')
            .getContext('2d');
            this.chart = new window.Chart(ctx, this.config);

            getFinancialAccounts({accountId: this.recordId, trend: 12, isWM: true})
            .then(result => {
                for(var key in result){
                    this.monthArray.push(key);
                    
                    for(var innerKey in result[key]){
                        if(innerKey == 'InvestmentAccount'){
                            this.bancaArray.push(result[key][innerKey]);
                        } else if(innerKey == 'PCI'){
                            this.depositArray.push(result[key][innerKey]);
                        } else if(innerKey == 'Structured_Deposit'){
                            this.wealthArray.push(result[key][innerKey]);
                        }
                    }
                }
                this.chart.update();
            })
            .catch(error => {
                
            });
        })
        .catch(error =>{
            this.dispatchEvent(
                new ShowToastEvent({
                    title : 'Error loading ChartJS',
                    message : error.message,
                    variant : 'error',
                }),
            );
        });
    }
}