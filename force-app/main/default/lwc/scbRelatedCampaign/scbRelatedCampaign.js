import { LightningElement, api, track } from 'lwc';

export default class scbRelatedCampaign extends LightningElement {
    @api recordId;

    @track campaignColumns = [
        {
            label: 'Name',
            fieldName: 'Campaign_Name',
            type: 'customurl',
            typeAttributes: {
                urlLink: {
                    fieldName : 'Campaign_LinkName'
                },
                isAccessible: {
                    fieldName : 'CampaignId_isVisible'
                },
                restrictAccess: false,
                objectName: 'Campaign',
                recordId: {
                    fieldName : 'CampaignId'
                }
            }
        },
        { label: 'Start Date', fieldName: 'Campaign_StartDate', type:"date-local", typeAttributes:{month:"2-digit", day:"2-digit"} },
        { label: 'End Date', fieldName: 'Campaign_EndDate', type:"date-local", typeAttributes:{month:"2-digit", day:"2-digit"} },
        { label: 'Active', fieldName: 'Campaign_IsActive', type: 'boolean'}
    ]
    accountId
    customActions = [{ label: 'Custom action', name: 'custom_action' }]

    renderedCallback(){
        this.accountId = this.recordId
    }
}
