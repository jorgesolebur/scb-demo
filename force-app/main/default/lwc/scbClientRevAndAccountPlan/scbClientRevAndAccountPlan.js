import { LightningElement, api, track } from 'lwc';
import labelTab from '@salesforce/label/c.SCB_Client_Review_Account_Plan';

export default class ScbClientRevAndAccountPlan extends LightningElement {
    @api recordId;

    label = {
        labelTab
    };
}