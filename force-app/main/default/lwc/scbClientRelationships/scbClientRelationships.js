import { LightningElement, api, wire, track } from 'lwc';

const columns = [
    {
        label: 'Relation No.',
        fieldName: 'FinServ__RelatedContact__r_Account_Relationship_No__c',
        type: 'customurl',
        typeAttributes: {
            urlLink: {
                fieldName : 'FinServ__RelatedContact__r_LinkName'
            },
            isAccessible: {
                fieldName : 'FinServ__RelatedContact__c_isVisible'
            },
            restrictAccess: true,
            objectName: 'Account',
            recordId: {
                fieldName : 'FinServ__RelatedContact__r_AccountId'
            }
        }
    },
    { label: 'Full Name', fieldName: 'Full_Name__c', hideDefaultActions: true },
    { label: 'Relation', fieldName: 'Relationship__c', hideDefaultActions: true }
];

export default class scbClientRelationships extends LightningElement {
    @api recordId;
    columns = columns;
    contactId;
    
    renderedCallback(){
        this.contactId = this.recordId
    }
}
