import { LightningElement, api } from 'lwc';

import getClient from '@salesforce/apex/SCB_LWCAccountController.getClient'; // to be removed

import { createMessageContext, releaseMessageContext, publish } from 'lightning/messageService';
import ServiceComplaintLMS from "@salesforce/messageChannel/ServiceComplaintLMS__c";

const columns = [
    { label: 'Service No.', fieldName: 'url', type: 'url', typeAttributes: { label : { fieldName: 'name'}}},
    { label: 'Status', fieldName: 'status'},
    { label: 'Service Type', fieldName: 'servicetype'},
    { label: 'Priority', fieldName: 'priority'}
];

export default class ScbRelatedServiceRequest extends LightningElement {
    @api recordId;
    @api gender; //temporary flag for sample purposes
    columns = columns

    data = [
        { id: "00000001", name: "00000001", status: "Pending", servicetype: "Credit Card - Card Cancellation", priority: "Low", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000002", name: "00000002", status: "Pending", servicetype: "Credit Card - Card Cancellation", priority: "Medium", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000003", name: "00000003", status: "On going", servicetype: "New STP - Credit Card Fee Waiver", priority: "High", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000004", name: "00000004", status: "Completed", servicetype: "New STP - Credit Card Fee Waiver", priority: "Low", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000005", name: "00000005", status: "Cancelled", servicetype: "Credit Card - Card Cancellation", priority: "High", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000006", name: "00000006", status: "Pending", servicetype: "Credit Card - Card Cancellation", priority: "High", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000007", name: "00000007", status: "Completed", servicetype: "Credit Card - Card Cancellation", priority: "Low", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000008", name: "00000008", status: "Completed", servicetype: "New STP - Credit Card Fee Waiver", priority: "High", url: "https://www.sc.com/hk/personal/reachus/apply"},
        { id: "00000009", name: "00000009", status: "Completed", servicetype: "New STP - Credit Card Fee Waiver", priority: "High", url: "https://www.sc.com/hk/personal/reachus/apply"}
    ]

    context = createMessageContext();

    // testing purposes only, event will be sent based on webservice callout result
    connectedCallback(){

        if(this.recordId !== undefined){
            getClient({recId : this.recordId}) 
            .then(data => {
                this.gender = data.accRec.FinServ__Gender__pc;
                // publish lightning message
                const message = {
                    servicecomplaintfound: data.accRec.FinServ__Gender__pc == "Male"
                };
                publish(this.context, ServiceComplaintLMS, message);
            })
            .catch(error => {});
        }
    }

    disconnectedCallback() {
        releaseMessageContext(this.context);
    }
}