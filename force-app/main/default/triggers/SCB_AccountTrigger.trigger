trigger SCB_AccountTrigger on Account (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	if(SCB_QueryWithoutSharing.getTriggerControl('SCB_AccountTrigger').Active__c){
	    SCB_AccountTriggerClass.executeTriggerEvents(trigger.isBefore, trigger.isAfter, 
	                                                  trigger.isInsert, trigger.isUpdate,trigger.isDelete, trigger.isUndelete, 
	                                                  trigger.oldMap, trigger.new, trigger.newMap);
	}
}