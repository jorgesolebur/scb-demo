({
    doInit: function(cmp) {
        var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({

            "url": $A.get("$Label.c.SCB_Quick_Create_Link_Service_Request")
            });
            urlEvent.fire();

            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            setTimeout(function(){ 
                dismissActionPanel.fire() 
            }, 500);
    }
})