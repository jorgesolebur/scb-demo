({
    createNew : function(component, event) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Account_Plan__c",
            "defaultFieldValues": {
                'Client_Name__c' : component.get("v.clientReview.Client_Name__c"),
                'Client_Review_Ref__c' : component.get("v.recordId")
            }
        });
        createRecordEvent.fire();
    }
})