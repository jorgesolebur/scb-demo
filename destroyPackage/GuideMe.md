Post-deployment steps for new environment (Note: This is to assume that all applications [ex. managed packaged apps, custom objects and fields etc.] are already installed and setup in the target environment before executing the steps below)

1. Pull latest components from develop branch in vs code.
2. Update destructiveChanges.xml by manually adding the components to be deleted if there's any. This file is located in destroyPackage\postDestroy folder. 
3. Perform deletion of components by opening a new terminal from vs code and executing the command below: 
sfdx force:mdapi:deploy -d folderName -u "TargetEnvironmentAlias" -w -1

Example: 
sfdx force:mdapi:deploy -d destroyPackage\postDestroy -u "sandboxDev" -w -1

4. Check the target environment if all components were deleted.
5. Commit the updated destructiveChanges.xml in GIT branch.